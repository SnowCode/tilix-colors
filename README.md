# Tilix colors
This repo contains a pack of color schemes for the Tilix terminal on Linux.

## Setup
1. Install Tilix (if you haven't already)

```bash
sudo apt-get install tilix
```

2. Download schemes and move them to the right directory

```bash
git clone https://codeberg.org/SnowCode/tilix-colors.git
mv tilix-colors/*.json ~/.config/tilix/schemes
rm -r tilix-colors
```

3. Change the tilix settings

```bash
tilix --preferences
```

4. Go into your current profile > Colors > then select the color scheme you want.
5. Restart Tilix

```bash
tilix
```

## Credits
* Go check out the [Original Project](https://github.com/storm119/Tilix-Themes) and star it for support, you can find a video and screenshots of most of the color schemes here.
